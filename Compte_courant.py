""" Enfant de Comptes_simple """

from Compte_simple import Comptes_simple

class Comptes_courant(Comptes_simple):

    def __init__(self, titulaire, solde):
        super().__init__(titulaire, solde)
        self.historique = []

    def debiter_compte(self, debit):
        super().debiter_compte(debit)
        self.historique.append(debit)

    def crediter_compte(self, credit):
        super().crediter_compte(credit)
        self.historique.append(credit)

    def relever_historique(self):
        for action in self.historique:
            print(action)
            