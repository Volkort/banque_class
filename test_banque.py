"""import pytest"""
from banque import Banque
from Compte_simple import Comptes_simple
from Compte_courant import Comptes_courant
from titulaire import Personne

def test_Banque():
    """TODO"""
    bnp = Banque("BNP")
    bnp.creation_compte("Jules", 6530)
    bnp.creation_compte("Xavier", 7302)
    caisse_epargne = Banque("Caisse d'epargne")
    caisse_epargne.creation_compte("Florent", 4358)
    total1 = bnp.total_solde()
    total2 = caisse_epargne.total_solde()
    
    assert bnp.nom == "BNP"
    assert caisse_epargne.nom == "Caisse d'epargne"
    assert total1 == 13832
    assert total2 == 4358

    bnp.prelevement_frais(100)
    total1 = bnp.total_solde()

    assert total1 == 13632

def test_Comptes_simple():
    """TODO"""
    jules = Comptes_simple("Jules", 2500)
    florent = Comptes_simple("Florent", 4862)
    john = Comptes_simple("John", -652)


    assert john.solde == -652
    assert jules.titulaire == Personne("Jules")
    assert florent.solde == 4862

    jules.debiter_compte(500)
    florent.crediter_compte(469)
    john.crediter_compte(1320)

    assert jules.solde == 2000
    assert florent.solde == 5331
    assert john.solde == 668

def test_Personne():
    """TODO"""
    sylvain = Personne("Sylvain")

    assert sylvain.nom == "Sylvain"

    sylvain.nom = "Mady"

    assert sylvain.nom != "Sylvain"
    assert sylvain.nom == "Mady"

def test_Comptes_courant():
    bnp = Banque("BNP")
    bnp.creation_compte_courant("Jules", 2500)
    bnp.dict_compte[10001].crediter_compte(4000)
    assert bnp.total_solde() == 6500
    bnp.creation_compte("Florent", 8000)
    assert bnp.total_solde() == 14500
    bnp.prelevement_frais(100)
    assert bnp.total_solde() == 14300
    bnp.relever_compte(bnp.dict_compte[10001])
