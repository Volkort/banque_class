"""Module qui definit une classe pour generer des comptes simples"""

from titulaire import Personne


class Comptes_simple:
    def __init__(self, titulaire = None, solde = 0) :
        self.titulaire = Personne(titulaire)
        self.__solde = solde
    
    @property
    def solde(self):
        return self.__solde
    
    @solde.setter
    def solde(self,solde):
        self.__solde = solde

    def __repr__(self):
        return "Comptes_simple({}, {})".format(self.titulaire, self.solde)

    def __str__(self):
        return "Titulaire du compte : {}, solde : {}".format(self.titulaire, self.solde)
    
    def debiter_compte(self, debit):
        self.solde -= debit

    def crediter_compte(self, credit):
        self.solde += credit

    def __eq__(self, autre):
        return self.titulaire == autre.titulaire and self.solde == autre.solde

        
if __name__ == "__main__":
    pass
