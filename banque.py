""" Module qui definit une classe pour generer un banque"""
from Compte_simple import Comptes_simple
from Compte_courant import Comptes_courant


class Banque:

    def __init__(self, nom = "default"):
        self.nom = nom
        self.dict_compte = {}
        self.nom_compte = 10001
    
    def creation_compte(self, titulaire, solde):
        self.dict_compte[self.nom_compte] = Comptes_simple(titulaire, solde)
        self.nom_compte += 1
    
    def creation_compte_courant(self, titulaire, solde):
        self.dict_compte[self.nom_compte] = Comptes_courant(titulaire, solde)
        self.nom_compte += 1
        
    def total_solde(self):
        total_solde = 0
        for key, compte in self.dict_compte.items() :
            total_solde += compte.solde
        return total_solde
    
    def prelevement_frais(self, frais):
        for numeros, compte in self.dict_compte.items() :
            compte.debiter_compte(frais)

    def relever_compte(self, compte):
        compte.relever_historique()
   